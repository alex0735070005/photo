import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Router from './Router';
import Menu from './Menu';
import './App.scss';

const App = () => {
    return (
      <BrowserRouter>
        <div className="App">
          <Menu/> 
          <Router/>
        </div>
      </BrowserRouter>
    );
}
export default App;
