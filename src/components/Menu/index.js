import React from 'react';
import { NavLink } from 'react-router-dom';
import data from './menu.json';
import './style.scss';

const Menu = () => {
    return (
        <ul className='menu'>
            {
                data.map(({ name, link }) =>
                    <li key={link}>
                        <NavLink
                            exact
                            to={link}
                        >
                            {name}
                        </NavLink>
                    </li>
                )
            }
        </ul>
    )
}

export default Menu;