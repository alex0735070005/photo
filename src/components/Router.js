import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Photos from './Photos';
import Favorites from './Favorites';

export default function () {
    return (
        <Switch>
            <Route exact path='/' component={Photos} />
            <Route path='/favorites' component={Favorites} />
        </Switch>
    )
}