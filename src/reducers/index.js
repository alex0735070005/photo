import { combineReducers } from 'redux'
import favorites from './favorites';
import photos from './photos';

export default combineReducers({
    favorites,
    photos,
})