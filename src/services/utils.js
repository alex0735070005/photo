export const getParamsString = (params, baseUrl) => {
    const keys = Object.keys(params);
    if (!keys.length) {return baseUrl};

    return keys.reduce((accom, key, index) => {
        let param = `${key}=${params[key]}`;
        if(index === keys.length -1) {
            return accom += param;
        }
        return accom += `${param}&&`;
    }, `${baseUrl}/?`);
}